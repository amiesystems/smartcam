var amqp = require('amqplib/callback_api');

var CommonF = require('./CommonFunctions.js');

var DEBUG = true;

var mainQue = 'rpc_main';
var replyToQue = 'amq.rabbitmq.reply-to';
//var replyToQue = 'new_reply';

var rulesChannel;

var RpcQuery = require('../resource.json').RpcQuery;

function startRabbitmq(que, callback){
	CommonF.log(DEBUG,"Connecting to RabbitMQ broker..");
	//Connect to rabbitmq broker
	amqp.connect('amqp://localhost', function(err, connection){
		connection.createChannel(function(err, channel){
			//Create rules que
			channel.assertQueue(que, {durable: false});
			channel.prefetch(1);	//Fetch only one task at a time
			rulesChannel = channel;
			CommonF.log(DEBUG, "...connected!")
			return callback(channel);
		});
	});
}

function sendSetStatusToMain(channelId, value, target){
	//Construct command object
	var command = CommonF.clone(RpcQuery.setStatus);
	command.message.channelId = channelId;
	command.message.value = value;
	command.message.target = target;
	 
	//Send command to main que
	rulesChannel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)));
}

function getCameraUrls(callback){
	//Construct command object
	var command = CommonF.clone(RpcQuery.getCameraUrls);
	
	//Set listners for reply to que
	rulesChannel.consume(replyToQue, function(msg){
		CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
		//Close consumer
		rulesChannel.cancel(msg.fields.consumerTag);
		//Return to caller with response received from ruleEngine
		return callback(JSON.parse(msg.content).message.urls);
	},
	{noAck: true},
	function(err,options){
		//Send command to main
		rulesChannel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
			{replyTo: replyToQue});
	});
}

module.exports.startRabbitmq = startRabbitmq;
module.exports.sendSetStatusToMain = sendSetStatusToMain;
module.exports.getCameraUrls = getCameraUrls;
