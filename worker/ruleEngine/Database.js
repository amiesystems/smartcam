var sqlite3 = require('sqlite3').verbose();
var db;
var moment = require('moment');
moment().format();
var async = require('async');

var CommonF = require('./CommonFunctions.js');

var RpcResponse = require('../resource.json').RpcResponse;

const DEBUG = true;

var activeRuleDevices = {};		//List of devices already having an active rule.

var insertRule;
var deleteRule;
var insertPrevStatus;

function startDb(){
	CommonF.log(DEBUG,"Initializing database..");
	//Create instance of database class
	db = new sqlite3.Database('RulesDB.db');
	//Create rules table if not exists
	db.run("Create table if not exists Rules (RuleId TEXT PRIMARY KEY, ChannelId TEXT, "
			+ "Event TEXT, Count INT, StartTime TEXT, StopTime TEXT, Days TEXT, "
			+ "Action TEXT, ActValue TEXT, Target TEXT, Status TEXT default NULL, NotifOption TEXT, PhoneNumber TEXT)"
	);
	
	//Prepare statements for rules table
	insertRule = db.prepare("Insert or replace into Rules (RuleId, ChannelId, Event, "
					+ "Count, StartTime, StopTime, Days, Action, ActValue, Target, NotifOption, PhoneNumber) "
					+ "values(?,?,?, ?,?,?, ?,?,?, ?,?,?)"
				);
	deleteRuleFromDb = db.prepare("Delete from Rules where RuleId = (?)");
	
	//Create Previous status table if not exists
	db.run("Create table if not exists PrevStatus (ID TEXT PRIMARY KEY, Status INT)");
	
	//Prepare statements for PrevStatus table
	insertPrevStatus = db.prepare("Insert or replace into PrevStatus (ID, Status) values (?,?)");

}

function editRule(rule, callback){
	var response = CommonF.clone(RpcResponse.saveRule);
	if(rule.ruleId == '' || rule.ruleId == undefined){		//New rule
		createRuleId(function(err,id){
			if(err){
				console.log("Error while creating rule id");
				return callback(response);
			}
			else{
				insertRule.run(id, rule.channelId, rule.event, 
					parseInt(rule.count), rule.startTime, rule.stopTime, 
					rule.selectedDays.toString(), rule.selectedAction, rule.actValue, 
					rule.target, rule.selectedNotifOption, rule.phoneNumber);
				response.message.ruleId = id;
				response.response = true;
				return callback(response);
			}
		});
	}
	else{		//Edit rule
		insertRule.run(rule.ruleId,rule.channelId, rule.event, 
			parseInt(rule.count), rule.startTime, rule.stopTime, 
			rule.selectedDays.toString(), rule.selectedAction, rule.actValue, 
			rule.target, rule.selectedNotifOption, rule.phoneNumber);
		response.message.ruleId = rule.ruleId;
		response.response = true;
		return callback(response);
	}
}


function createRuleId(callback){
	var newRuleId;
	db.get('select max(RuleId) as id from Rules', function(err,row){
		if(err){
			return callback(err);
		}
		else{
			if(row.id == null){
				newRuleId = 1;
			}
			else{
				newRuleId = parseInt(row.id.substring(1)) + 1;
			}
			//Below equation formats the number to a 3 digit string(eg: 3-> '003')
			newRuleId = ("00" + Number(newRuleId)).slice(-3).toString();
			//Add prefix 'r'
			newRuleId = 'r' + newRuleId;
			return callback(null,newRuleId);
		}
	});
}

function findMatchingRules(channelId,triggerArray,callback){
	var combinedRuleList = [];	//List of all rules applicable for current scene 
	async.eachSeries(triggerArray,function(trigger,doneCallback){
		getAllMatchingRules(channelId, trigger.event, trigger.count, function(err,matchedRules){				//Stage 1	getAllMatchingRules(channelId, event, index, callback)
			if(err){
				return doneCallback(null);
			}else if(matchedRules.length <=1){						//Filter only if more than 1 rule found for an event
				//Push each row in matchedRules array to the combinedRules array
				combinedRuleList = combinedRuleList.concat(matchedRules);
				return doneCallback(null);
			}
			else{													//Filter only if more than 1 rule found for an event
				//console.dir(matchedRules);
				//filterRulesWithMaxCount implements logic for filtering out irrelevent rules if more than one matched.
				filterRulesWithMaxCount(matchedRules,function(filteredRules){										//Stage 2
					//Push each row in filteredRules array to the combinedRules array
					combinedRuleList = combinedRuleList.concat(filteredRules);
					return doneCallback(null);
				});
			}
		});
	},
	function(err){
		//findRuleStatus segregates rules into two lists - one which needs to be activated, and another which needs to be
		//deactivated. It does not return rules which are already activated and still valid..
		findRuleStatus(channelId, combinedRuleList, function(activateRules, deactivateRules){						//Stage 3
			//If both activate and deactivate lists are not empty
			//pass them to stage 4 and filter out rules with same target
			//Else return current list

			if(activateRules.length > 0 && deactivateRules.length > 0){
				filterRulesToSameTarget(activateRules,deactivateRules,function(activateRules, deactivateRules){	//Stage 4
					return callback(activateRules, deactivateRules);
				});
			}
			else{
				return callback(activateRules, deactivateRules);
			}
		});
	});
}
function getAllMatchingRules(channelId, event, count, callback){		//Stage 1
	var matchedRules = [];
	var days = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
	var now = new Date();
	//Select all rules matching event and count conditions from database
	db.all('select * from Rules where ChannelId = ? and Event = ? and Count <= ?',
		channelId,event,count,function(err, rows){
			if(err){
				return callback(err);
			}
			else if(rows){
				var todaysDay = days[now.getDay()];
				for(var i = 0; i<rows.length;i++){
					var rule = rows[i];
					var thisRuleValidNow = false;
					if(rule.Days.includes(todaysDay)){
						//now check if the time is within the given time frame..
			
						//change in rule time check algorithm..
						var currentTimeMins = parseInt(now.getHours())*60 + parseInt(now.getMinutes());
						var startTimeMins = parseInt(rule.StartTime.substring(0,2))*60 + parseInt(rule.StartTime.substring(3,5));
						var stopTimeMins = parseInt(rule.StopTime.substring(0,2))*60 + parseInt(rule.StopTime.substring(3,5));
						if(stopTimeMins > startTimeMins){	//time period not crossing midnight
							if(stopTimeMins > currentTimeMins && currentTimeMins >= startTimeMins){
								thisRuleValidNow = true;
							}
						}else if(stopTimeMins < startTimeMins){//time period crossing midnight
							if(currentTimeMins > startTimeMins || currentTimeMins < stopTimeMins){
								thisRuleValidNow = true;
							}
						}
					}else{

					}
					//console.log(rule.RuleId + " >> valid now?  >> " + thisRuleValidNow);
					if(thisRuleValidNow){
						matchedRules.push(rows[i]);
					}
				}
				// //Iterate through all rows 
				// //and filter rules matching day, start time and stop time
				// //based on current timestamp
				// for(var i=0; i<rows.length; i++){
				// 	if(rows[i].Days.indexOf(currentDay) > -1){									//check with selected days of rule.
				// 		console.log(rows[i].RuleId + "Current day is there in the rule days - satisfied... CurrentDay = " + currentDay + " Ruledays = " + rows[i].Days);
						

						// if(currentTime.isSameOrAfter(moment(rows[0].StartTime,'HH:mm'))){		//check start time
						// 	console.log(rows[i].RuleId + " - current time is same or after start time is matched")
						// 	if(currentTime.isSameOrBefore(moment(rows[0].StopTime,'HH:mm'))){	//Check stop time
						// 		console.log(rows[i].RuleId + " - currentTime is same or before stop time is matched.")
						// 		matchedRules.push(rows[i]);										//push rule to an array if all conditions satisfied
						// 	}
						// }
				

				// 	}
				// }
				//console.log("MatchedRules count ------->>>>>>>>>>>>" + matchedRules.length);
				return callback(null, matchedRules);											//Return array of matched rules
			} else{
				//console.log("no matching rows in getAllMatchingRules/....................>>>>>>>>>>");
				return callback("no matching rows", null);
			}
		}
	);
}

//if more than one rules matched, filter and return only relevent rules.. viz. rules with maximum 'count'
function filterRulesWithMaxCount(rules,callback){		//Stage 2
	var filteredRules = [];
	var maxCount;
	
	//Sort rules array in descending based on Count
	rules.sort(function(a,b){
		return b.Count - a.Count;
	});
	
	//Filter rules with maximum count
	//Count property of 0th row will be the highest count
	//If array length is 0 take maxCount=0
	if(rules.length > 0){
		maxCount = rules[0].Count;
	}
	else{
		maxCount = 0;
	}
	//Filter all rows from array with Count property equals to maxCount
	filteredRules = rules.filter(function(a){
		return (a.Count==maxCount);
	});
	
	return callback(filteredRules);
}

//currentSceneRules are the rules which are extracted for this iteration of trigger..
function findRuleStatus(channelId, currentSceneRules, callback){	//Stage 3
	var activateRules = [];
	var continueRules = [];
	var deactivateRules = [];
	
	//Get list of rules with status ACTIVATE or CONTINUE
	//If a rule in rules array matches with rows returned from database
	//Push that rule to list of rules to be continued
	//If no rule in rules array matches with with a row returned from database
	//Push that row to list of rules to be deactivated
	db.all('select * from Rules where ChannelId = (?) and (Status = (?) or Status = (?))',channelId,'ACTIVATE','CONTINUE',function(err,activeRules){
		if(err){
			
		}
		else{
			for(var i=0; i<activeRules.length; i++){
				for(var j=0; j<currentSceneRules.length; j++){
					if(currentSceneRules[j].RuleId == activeRules[i].RuleId){			//rule in current scene is already activate, push it to continueRules list
						continueRules.push(currentSceneRules[j]);
						break;
					}
				}
				if(j >= currentSceneRules.length){										//completed iteration through rules in current scene, no match with activated rules, 
					deactivateRules.push(activeRules[i]);								//push activated rule to deactivateRules list
				}
			}
		}
		
		//Get list of rules with status DEACTIVATE or NULL
		//If a rule in rules array matches with rows returned from database
		//Push that rule to list of rules to be activated
		db.all('select * from Rules where ChannelId = (?) and (Status = (?) or Status is null)',channelId,'DEACTIVATE',function(err,inactiveRules){
			if(err){
				
			}
			else{
				for(var i=0; i<inactiveRules.length; i++){
					for(var j=0; j<currentSceneRules.length; j++){
						if(currentSceneRules[j].RuleId == inactiveRules[i].RuleId){		//rule in current scene is currently inactive, push it to activateRules list
							activateRules.push(currentSceneRules[j]);
							break;
						}
					}
					//completed iteration through rules in current scene
					//Do nothing with inactive rules if they do not match any rules in current scene
				}
			}
			saveRuleStatus(activateRules,continueRules,deactivateRules);
			return callback(activateRules,deactivateRules);
		});
	});
}

/*
 * If a rule to the same target is present at activateRules list and deactivateRules list
 * remove that rule from deactivateRules list
 */
function filterRulesToSameTarget(activateRules, deactivateRules, callback){		//Stage 4
	var keepRule = true;
	var newDeactivateList = [];
	for(var i=0; i<deactivateRules.length; i++){
		for(var j=0; j<activateRules.length; j++){
			if(activateRules[j].Target == deactivateRules[i].Target){
				keepRule = false;
				break;
			}
		}
		if(keepRule == true){
			newDeactivateList.push(deactivateRules[i]);
		}
		else{
			//Do not add this rule to new list
			keepRule = true;	//set keepRule to true for next iteration
			
			//Keep a list of devices which have already an active rule.
			//Do not store the current status of these devices in database after new rule activation
			activeRuleDevices[deactivateRules[i].Target] = true;
		}
	}
	return callback(activateRules, newDeactivateList);
}
//set status of rules to ACTIVATE, CONTINUE, or DEACTIVATE
function saveRuleStatus(activateRules, continueRules, deactivateRules){
	var query;
	if(activateRules.length > 0){
		query = 'update Rules set Status = "ACTIVATE" where';
		//construct query string
		for(var i=0; i<activateRules.length; i++){
			if(i == 0){
				query = query + ' RuleId = "' + activateRules[i].RuleId + '"';
			}
			else{
				query = query + ' or RuleId = "' + activateRules[i].RuleId + '"';
			}
		}
		db.run(query,function(err){
			//console.log(err);
		});
	}	
	if(continueRules.length > 0){
		query = 'update Rules set Status = "CONTINUE" where';
		//construct query string
		for(var i=0; i<continueRules.length; i++){
			if(i == 0){
				query = query + ' RuleId = "' + continueRules[i].RuleId + '"';
			}
			else{
				query = query + ' or RuleId = "' + continueRules[i].RuleId + '"';
			}
		}
		db.run(query,function(err){
			//console.log(err);
		});
	}	
	if(deactivateRules.length > 0){
		query = 'update Rules set Status = "DEACTIVATE" where';
		//construct query string
		for(var i=0; i<deactivateRules.length; i++){
			if(i == 0){
				query = query + ' RuleId = "' + deactivateRules[i].RuleId + '"';
			}
			else{
				query = query + ' or RuleId = "' + deactivateRules[i].RuleId + '"';
			}
		}
		db.run(query,function(err){
			//console.log(err);
		});
	}	
}

function saveTargetStatus(status, target, callback){
	//console.log("inside Database.saveTargetStatus");
	if(activeRuleDevices[target] == true){

		//console.log(target + " is an active rule's target, so not updating previous status");

		//Do not save current status for this device, since the first setState will be triggered by smart cam activation.
		
		//make it false so that only the first setState is neglected immediately after a trigger by smart cam. 
		//should save from the next setState onwards since the trigger may be any other source, like switch, app, schedule etc.
		activeRuleDevices[target] = false;
		return callback(null);		//NULL or error
	}
	else{
		//console.log(target + " is NOT any active rule's target device.. so updating in db");
		insertPrevStatus.run(target, status, function(err){
			return callback(err);		//NULL or error
		});
	}
}

function deleteRule(ruleId, callback){
	deleteRuleFromDb.run(ruleId, function(err){
		return callback(err);		//NULL or error
	});
}
//get previous status of a given target
function getPrevStatus(target,i, callback){
	//console.log("inside getPrevStatus, value of target>>>");
	//console.dir(target);
	db.get('select Status from PrevStatus where ID = (?)',target, function(err, row){
		if(err){
			return callback(err,null,i);
		}
		else if(row == undefined){
			return callback(new Error("Previous status not recorded"),null,i);
		}
		else{
			return callback(null,row.Status,i);
		}
	});
}

function getAllRules(channelId, callback){
	var response = CommonF.clone(RpcResponse.getAllRules);
	db.all('select * from Rules where ChannelId = (?)', channelId, function(err,rows){
		if(err){
			response.message.rules = [];
		}
		else{
			response.message.rules = rows;
		}
		response.response = true;
		return callback(response);
	});
}


function getAllTargets(callback){
	var feedback = CommonF.clone(RpcResponse.getAllTargets);
	db.all('select Target from Rules where Target is not null',function(err, rows){
		if(err){
			feedback.message.targets = [];
		}else{
			feedback.message.targets = rows;
		}
		feedback.response = true;
		return callback(feedback);
	});
}

function getRule(ruleId, callback){
	var response = CommonF.clone(RpcResponse.getRule);
	db.get('select * from Rules where RuleId = (?)', ruleId, function(err,row){
		if(err || row == undefined){
			response.response=false;
		}
		else{
			response.message.ruleId = row.RuleId;
			response.message.channelId = row.ChannelId;
			response.message.event = row.Event;
			response.message.count = row.Count;
			response.message.startTime = row.StartTime;
			response.message.stopTime = row.StopTime;
			response.message.selectedDays = row.Days;
			response.message.selectedAction = row.Action;
			response.message.actValue = row.ActValue;
			response.message.target = row.Target;
			response.message.selectedNotifOption = row.NotifOption;
			response.message.phoneNumber = row.PhoneNumber;
			response.response = true;
		}
		return callback(response);
	});
}

module.exports.startDb = startDb;
module.exports.editRule = editRule;
module.exports.findMatchingRules = findMatchingRules;
module.exports.saveTargetStatus = saveTargetStatus;
module.exports.getPrevStatus = getPrevStatus;
module.exports.getAllRules = getAllRules;
module.exports.getRule = getRule;
module.exports.deleteRule = deleteRule;
module.exports.getAllTargets = getAllTargets;
