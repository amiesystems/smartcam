var fs = require('fs');
const spawn = require('child_process').spawn;
const exec = require('child_process').exec;
var async = require('async');

var CommonF = require('./CommonFunctions.js');
var Database = require('./Database.js');
var Communication = require('./RulesComm.js');
var TextNotify = require('./TextNotify.js');

const DEBUG = true;
const ErrorLog = true;
const InfoLog = true;

const config = require('../config.json');
const maxChannels = config.Channels;
const deviceId = config.DeviceId;
const triggers = config.Triggers;

var rulesQue = 'rpc_rules';
var mainQue = 'rpc_main';
var replyToQue = 'amq.rabbitmq.reply-to';

var RpcQuery = require('../resource.json').RpcQuery;
var RpcResponse = require('../resource.json').RpcResponse;


/*********Initialize database***********/
Database.startDb();

/****************RabbitMQ Communication**********************/
Communication.startRabbitmq(rulesQue,function(rulesChannel){
	//Listen to rules que
	rulesChannel.consume(rulesQue, function(msg){
		CommonF.log(DEBUG,'Received RPC message', CommonF.strTruncate(msg.content));
		var req = JSON.parse(msg.content);
		switch(req.action){
			case RpcQuery.saveRule.action:
				Database.editRule(req.message,function(response){
					rulesChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					rulesChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to main', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.currentStatus.action:
				Database.saveTargetStatus(req.message.currentValue, req.message.target, function(err){
					if(err){
						rulesChannel.nack(msg);		//If err reject message and reque it
					}
					else{
						rulesChannel.ack(msg);		//If no error acknowledge message
					}
				});
				break;
			case RpcQuery.getAllRules.action:
				Database.getAllRules(req.message.channelId,function(response){
					rulesChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					rulesChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to main', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.getAllTargets.action:
				Database.getAllTargets(function(response){
					rulesChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					rulesChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to main', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			
			case RpcQuery.getRule.action:
				Database.getRule(req.message.ruleId,function(response){
					rulesChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					rulesChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to main', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.deleteRule.action:
				Database.deleteRule(req.message.ruleId, function(err){
					if(err){
						//console.log(err);
						rulesChannel.nack(msg);		//If err reject message and reque it
					}
					else{
						//console.log('ack');
						rulesChannel.ack(msg);		//If no error acknowledge message
					}
				});
				break;
			case RpcQuery.RestartRuleEngine.action:
				CommonF.log(DEBUG,"restarting rule engine");
				exec('sudo forever restart ruleEngine',function (error, stdout, sterr){
					if(error) console.log(error);
				});

				break;
		}
	});
	// //start ai cores after initializing RabbitMQ connection
	// startAiCores();
	//after setting up rabbit mq, loop through all channels in this processor and get rules inside them
	setInterval(function(){
		checkIfCoresShouldRunAndAct()},
		 5000);
});

function checkIfCoreShouldRunForChannel(channelId, callbackCheck){
	var days = ["SUN","MON","TUE","WED","THU","FRI","SAT"];
	var now = new Date();
	//console.log("checking if core should run for channel " + channelId);
	//get all rules for the channel id
	//console.log("getting all rules for channel " + channelId);
	Database.getAllRules(channelId, function(response){
		var aiShouldRun = false;
		var rules = response.message.rules;
		//console.dir(rules);
		async.each(rules, function(rule, callback){
			var todaysDay = days[now.getDay()];
			if(rule.Days.includes(todaysDay)){
				//now check if the time is within the given time frame..
				//ie, if the current time is greater than start time 
				// .. and less than end time...
				var currentTimeMins = parseInt(now.getHours())*60 + parseInt(now.getMinutes());
				var startTimeMins = parseInt(rule.StartTime.substring(0,2))*60 + parseInt(rule.StartTime.substring(3,5));
				var stopTimeMins = parseInt(rule.StopTime.substring(0,2))*60 + parseInt(rule.StopTime.substring(3,5));
				if(stopTimeMins > startTimeMins){	//time period not crossing midnight
					if(stopTimeMins > currentTimeMins && currentTimeMins >= startTimeMins){
						aiShouldRun = true;
						callback();
					}else{
						callback(); 
					}
				}else if(stopTimeMins < startTimeMins){//time period crossing midnight
					if(currentTimeMins > startTimeMins || currentTimeMins < stopTimeMins){
						aiShouldRun = true;
						callback();
					}else{
						callback(); 
					}
				}
			}else{
				callback();
			}
		}, function(err){
			if(err){
				throw err;
			}else{
				//console.log("check result: channel id:" + channelId + " AI should run:" + aiShouldRun);
				callbackCheck(aiShouldRun, channelId);
			}
		})
	});
}
var shouldAIRunNowForChannel=[];

function startOrStopChannel(channelIndex){
	if(channelIndex < maxChannels){
		var channelId = CommonF.getChannelId(channelIndex+1);		
		checkIfCoreShouldRunForChannel(channelId, function(coreShouldRun, checkedChannelId){
			if(coreShouldRun){
				startAiCore(checkedChannelId);
			}else{
				if(isAIRunningForChannel(checkedChannelId)){
					stopAiCore(checkedChannelId);
				}
			}
			//check next channel recursively
			//console.log("calling next channel check recursively");
			startOrStopChannel(channelIndex+1);
		});
	}else{
		//all channels checked
		//console.log("all channels checked..");
	}
}

function checkIfCoresShouldRunAndAct(){
	startOrStopChannel(0);
}


/*************************AI Core***************************/
var coreListeners = [];
//var aiCorePath = './aiCore.py';
var aiCorePath = '../../smartcam_core_ai/ai.py';
var model = '../../smartcam_core_ai/mob.caffemodel';
var prototxt = '../../smartcam_core_ai/mob.proto.txt';
var channelId;

function isAIRunningForChannel(channelId){
	if(coreListeners[channelId]){
		if(coreListeners[channelId].process){
			if(coreListeners[channelId].status == "Active"){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
function startAiCore(channelId){
	//console.log("startAiCore called... ");	
	if(coreListeners[channelId] && coreListeners[channelId].status == "Active"){
		//do nothing if this channel already active
		//console.dir(coreListeners[channelId]);
		if(coreListeners[channelId].process && coreListeners[channelId].process.killed){
			coreListeners[channelId].status == 'Inactive';
		}
	}else{
		Communication.getCameraUrls(function(urls){
			//start ai core for 
			coreListeners[channelId] = {
				process:'',
				status:'Inactive'
			};
			coreListeners[channelId].process = spawn('python3',[aiCorePath, '--index=' 
				+ channelId, '--link=' + urls[channelId], '--model=' + model, '--prototxt=' + prototxt]);
			CommonF.log(InfoLog,"AI core for channel " + channelId + " started.");
			coreListeners[channelId].process.stdout.on('data',function(data){
				//CommonF.log(DEBUG,'STDOut:',data.toString().trim());
				onTrigger(data.toString().trim());
			});
			coreListeners[channelId].process.stderr.on('data',function(data){
				CommonF.log(ErrorLog,'Error in Ai Core for the channel:' + channelId , data.toString().trim());
			});
			coreListeners[channelId].status = "Active";
		});
	}
}

function stopAiCore(channelId){
	if(coreListeners[channelId]&&coreListeners[channelId].status == "Active"){
		coreListeners[channelId].process.kill();
		coreListeners[channelId].status = "Inactive";
		CommonF.log(InfoLog,"AI core for channel " + channelId + "stopped.");
	}
}

var oldState = [];
//Call this function on triggers from AI core
function onTrigger(triggerObj){
	try{
		triggerObj = JSON.parse(triggerObj);
		CommonF.log(DEBUG,"Index:", JSON.stringify(triggerObj.index),"Contents:",JSON.stringify(triggerObj.contents));
		if(triggerObj.contents.length > 0){
			//Search for matching rules in database
			Database.findMatchingRules(triggerObj.index,triggerObj.contents, function(activateRules, deactivateRules){
				if(activateRules.length > 0){
					for(var i=0; i<activateRules.length; i++){
							setRule(activateRules[i],triggerObj.image.substring(2));//remove first two characters --> b' <-- which gets appended by python unnecessarily
					}
				}
				
				if(deactivateRules.length > 0){
					for(var i=0; i<deactivateRules.length; i++){
						revokeRule(deactivateRules[i]);
					}
				}
			});
		}
	}
	catch(err){
		CommonF.log(DEBUG,err);
		CommonF.log(DEBUG,triggerObj);
	}
}
var lastTriggeredTime = [];

//Call this function for each rule to be activated
function setRule(ruleObj,image){

	console.log("Action is : "+ruleObj.Action);
	var now = new Date();
	var difference;
	var differenceSeconds;
	
	if(lastTriggeredTime[ruleObj.RuleId]){
		difference = Math.abs(now - lastTriggeredTime[ruleObj.RuleId]);
		differenceSeconds = Math.ceil(difference/(1000));
	}else{
		differenceSeconds = 0;	//0 difference if first time triggered.
	}
	switch(ruleObj.Action){
		case "SET_STATUS":
			Database.getPrevStatus(ruleObj.Target,0, function(err,prevStat,iInCallBack){
			//clear any pending rule revoke action 
				if(oldStateSetTimeout[ruleObj.RuleId]){
					CommonF.log(DEBUG,"Clearing revoke's setTimeout for rule",ruleObj.RuleId);
					clearTimeout(oldStateSetTimeout[ruleObj.RuleId]);
					oldStateSetTimeout[ruleObj.RuleId] = null;
				}else{//if there is no settimeout running 
					//then update old state....
					if(err){
						oldState[ruleObj.RuleId] = 0;
					}else{
						oldState[ruleObj.RuleId] = prevStat;
					}
					storeImage(ruleObj,image);
					//and then send status to hub..
					CommonF.log(DEBUG,'>>> Setting ' + ruleObj.Target + ' to ' + ruleObj.ActValue, "Rule:",ruleObj.RuleId);
					Communication.sendSetStatusToMain(ruleObj.ChannelId, ruleObj.ActValue, ruleObj.Target);
				}
				lastTriggeredTime[ruleObj.RuleId] = now;//last triggered time should be updated everytime a rule is passed..
				//... irrespective of whether the oldstate settimeout is running or not.
			});
			break;
		case "NOTIFY":
			var minGapSeconds = config.MinimumGapMinutes.Notify*60;
			console.log("Notify differenceInSecs:" + differenceSeconds + " min gap: " + minGapSeconds);
			if(differenceSeconds > minGapSeconds || differenceSeconds == 0){
				if(ruleObj.NotifOption == "SMS"){
					//send the image to hub to be uploaded.. hub will send it to cloud for notifying the user 
					var currentTime = '';
					if(now.getHours()>12){
						currentTime = currentTime + (now.getHours() - 12).toString() + ":" 
										+ now.getMinutes() + ":" + now.getSeconds() + " PM";
					}else{
						currentTime = currentTime + now.getHours() + ":" 
										+ now.getMinutes() + ":" + now.getSeconds() + " AM";
					}
					var notifMessage
					if(ruleObj.Count == 1)
						notifMessage = "Amie detected one " + ruleObj.Event + " in your camera at " + currentTime;
					else
						notifMessage = "Amie detected " + ruleObj.Count + " " + ruleObj.Event + "s in your camera at " + currentTime;

					storeImage(ruleObj,image);
					var notifData = {
						image: image,
						message: notifMessage,
						number: ruleObj.PhoneNumber
					};
					console.log(notifMessage);
					TextNotify.sendSMS(ruleObj.PhoneNumber, notifMessage);
					lastTriggeredTime[ruleObj.RuleId] = now;
				}
			}else{
				console.log("!!!!!!!!!!<>>>>>>>>>> Not sending notification because time gap less..");
			}
			break;
		case "RECORD":
			storeImage(ruleObj,image);
			break;
	}
	
}


// //test
// getImageNamesOfChannel('Ruler003',files=>{
// 	console.dir(files);
// });

function storeImage(ruleObj, image){
	var now = new Date();
	var buf = new Buffer(image, 'base64');
	//CommonF.log(DEBUG,"Image content: ",buf);
	var imageFileName = "../main/public/Records/Images/" + ruleObj.ChannelId + "-" + ruleObj.RuleId + "-" + ruleObj.Event +"-"
		+ now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay() + "-" + now.getHours() + ":" 
		+ now.getMinutes() + ":" + now.getSeconds() + ".jpg";
	fs.writeFile(imageFileName, buf,"binary", (err) => {
		if (err) throw err;
		console.log('Image saved!');
	});
}
oldStateSetTimeout = [];
function setOldStateOfRule(ruleObj){
	if(oldState[ruleObj.RuleId]){
		CommonF.log(DEBUG,"Revoking state of",ruleObj.Target,"to",oldState[ruleObj.RuleId]);
		Communication.sendSetStatusToMain(ruleObj.ChannelId, oldState[ruleObj.RuleId], ruleObj.Target);
	}else{
		CommonF.log(DEBUG,"Revoking state of",ruleObj.Target,"to",oldState[ruleObj.RuleId]);
		Communication.sendSetStatusToMain(ruleObj.ChannelId, 0, ruleObj.Target);
	}
	oldStateSetTimeout[ruleObj.RuleId] = null;	//make settimeout object undefined..
}

//Call this function for each rule to be deactivated
function revokeRule(ruleObj){
	switch(ruleObj.Action){
		case "SET_STATUS":
			//wait for the minimum gap time before setting the group/scene to old state.. 
			var millisecs = config.MinimumGapMinutes.SetStatus*60*1000;
			CommonF.log(DEBUG,"revoking rule after " + millisecs + " millisecs..");
			oldStateSetTimeout[ruleObj.RuleId] = setTimeout(function(){setOldStateOfRule(ruleObj);}, millisecs);
			break;
	}	
}
