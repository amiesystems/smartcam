const spawn = require('child_process').spawn;
var fs = require('fs');
var async = require('async');

var CommonF = require('./CommonFunctions.js');
var Communication = require('./Communication.js');

const config = require('../config.json');
const maxChannels = config.Channels;

//Commands and Responses for smart camera device communication
var Commands = require('../resource.json').Commands;
var Responses = require('../resource.json').Responses;
var RpcResponse = require('../resource.json').RpcResponse;

const DEBUG = true;

/*
 * Try reading and parse channel info files.
 * if any error is thrown channel info files are currepted or not created
 * Create new files on error. Else do nothing
 */
function createChannelInfoFiles(){
	var fileContents;
	var channelId;
	var filePath = './channel_info/';
	for(var i=0; i<maxChannels; i++){
		try{
			channelId = CommonF.getChannelId(i+1);
			fileContents = fs.readFileSync(filePath + channelId + '.json');
			fileContents = JSON.parse(fileContents);
		}
		catch(err){
			CommonF.log(DEBUG, 'Channel info read error', err)
			fileContents = {};
			fileContents.channelId = channelId;
			fileContents.channelName = 'Channel ' + (i+1);
			fileContents.status = 'Online';
			fileContents.selectedCamera = '';
			fileContents.cameraUrl = '';
			writeChannelInfo(channelId,fileContents);
		}
	}
}

function writeChannelInfo(channelId,channelInfo,callback){
	var filePath = './channel_info/' + channelId + '.json';
	fs.writeFile(filePath, JSON.stringify(channelInfo,null,'\t'),function(err){
		if(err){
			CommonF.log(DEBUG, 'Channel info write error', err);
			if(callback && typeof(callback) == 'function'){
				return callback(err);
			}
		}
		else{
			CommonF.log(DEBUG, 'Wrote channel info', channelId);
			if(callback && typeof(callback) == 'function'){
				return callback(null);
			}
		}
	}); 
}

function readChannelInfo(channelId,callback){
	var filePath = './channel_info/' + channelId + '.json';
	fs.readFile(filePath,function(err,data){
		if(err){
			return callback(err);
		}
		else{
			try{
				var channelInfo = JSON.parse(data.toString());
				return callback(null,channelInfo);
			}
			catch(err){
				return callback(err);
			}
		}
	});
}

/* Read existing channel information 
 * and modify it if new valid values are supplied
 */ 
function saveChannelInfo(newInfo,callback){
	var response = CommonF.clone(RpcResponse.saveChannelInfo);
	readChannelInfo(newInfo.id,function(err, channelInfo){
		if(err){
			response.response = false;
			return callback(response);
		}
		else{
			if(newInfo.name != undefined && newInfo.name != null && newInfo.name != ''){
				channelInfo.channelName = newInfo.name;
			}
			if(newInfo.selectedCamera != undefined && newInfo.selectedCamera != null && newInfo.selectedCamera != ''){
				channelInfo.selectedCamera = newInfo.selectedCamera.ID;
				channelInfo.cameraUrl = newInfo.selectedCamera.url;
				//console.log("*** camera url received at smart cam" + channelInfo.cameraUrl);
			}
			else{
				channelInfo.selectedCamera = '';
				channelInfo.cameraUrl = '';
			}
			writeChannelInfo(newInfo.id,channelInfo,function(err){
				if(err){
					response.response = false;
				}
				else{
					response.response = true;
				}
				//restart rule engine after any change to channel info:
				

				//send response back to caller
				return callback(response);
			});
		}		
	});
}
//send file names of all images generated by the channel
function getImageNamesOfChannel(channelId, callback){
	var response = CommonF.clone(RpcResponse.getChannelImages);
	var imageFileNames = [];
	var files = fs.readdirSync("public/Records/Images/");
	var count = files.length;
	console.log("Files found in directory: " + count);
	var i = 0;
	files.forEach(file=>{
		if(file.indexOf(channelId) > -1){
			imageFileNames.push(file);
		}
		if(++i>=count){
			response.response = true;
			response.message.id = channelId;
			response.message.fileNames = imageFileNames;
			callback(response);
		}
	});
}

//Sends all channels info to hub's smart cam settings page.
//The frame url is used to display the content served by this channel..
//..  inside a frame in smart cam settings page of hub
function sendAllChannels(localIp){
	var payload = {};
	for(var i=0;i<maxChannels;i++){
		payload.id = CommonF.getChannelId(i+1);
		payload.name = 'Channel ' + (i+1);
		payload.status = 'Available';
		payload.frameUrl = `http://${localIp}:3001?channelId=${payload.id}`;
		Communication.publishResponse(Communication.Topics.getChannelListTopic, payload)
	}
}

//Fetch detail info about selected channel for displaying in settings page
function sendChannelInfo(channelId, callback){
	var response = CommonF.clone(RpcResponse.getChannelInfo);
	response.message.triggers = config.Triggers;
	readChannelInfo(channelId,function(err,channelInfo){
		if(err){
			response.response = false;
			return callback(response);
		}
		else{
			response.message.id = channelInfo.channelId;
			response.message.name = channelInfo.channelName;
			response.message.selectedCamera = channelInfo.selectedCamera;
			response.message.status = channelInfo.status;
			//response.message.rules = buildRuleStrings(channelInfo.rules);

			//the cameras to be listed in the settings page served by the smart cam module 
			//.. should be retrieved again from the main hub as below...
			Communication.getCameraList(channelInfo.channelId,function(cameras){
				response.message.cameras = cameras;
				Communication.getAllRules(channelInfo.channelId,function(rules){
					response.message.rules = rules;                                                                                                                                                      
					response.response = true;
					return callback(response);
				});
				/*Communication.getGroupList(channelInfo.channelId,function(groups,scenes){
					response.message.groups = groups;
					response.message.scenes = scenes;
					response.response = true;
					return callback(response);
				});*/
			});
		}
	});
}

function getInfoEditRule(ruleInfo,callback){
	var response = CommonF.clone(RpcResponse.getInfoEditRule);
	response.message.triggers = config.Triggers;
	response.message.actions = config.Actions;
	response.message.notifOptions = config.NotifOptions;
	Communication.getGroupList(ruleInfo.channelId,function(groups,scenes){
		response.message.groups = groups;
		response.message.scenes = scenes;
		//if no rule id
		if(ruleInfo.ruleId == undefined || ruleInfo.ruleId == null || ruleInfo.ruleId == ''){
			response.message.channelId = ruleInfo.channelId;
			response.response = true;
			return callback(response);
		}
		else{
			Communication.getRule(ruleInfo.ruleId, function(ruleObj){
				response.message.ruleId = ruleObj.ruleId;
				response.message.channelId = ruleObj.channelId;
				response.message.event = ruleObj.event;
				response.message.count = ruleObj.count;
				response.message.startTime = ruleObj.startTime;
				response.message.stopTime = ruleObj.stopTime;
				response.message.selectedDays = ruleObj.selectedDays.split(',');
				response.message.selectedAction = ruleObj.selectedAction;
				response.message.actValue = ruleObj.actValue;
				response.message.target = ruleObj.target;
				response.message.selectedNotifOption = ruleObj.selectedNotifOption;
				response.message.phoneNumber = ruleObj.phoneNumber;
				response.response = true;
				return callback(response);
			});
		}
	});
}

function buildRuleStrings(ruleObj){
	var ruleStrings = [];
	var triggers = config.Triggers;
	for(var i=0; i<triggers.length; i++){
		if(ruleObj[triggers[i]]){
			for(var j=0; j<ruleObj[triggers[i]].length; j++){
				ruleStrings.push(ruleObj[triggers[i]][j]);
			}
		}
	}
	return ruleStrings;
}

function sendCameraUrls(callback){
	var response = CommonF.clone(RpcResponse.getCameraUrls);
	var urls = {};
	var channels = [];
	//Create an array of channeIds
	//Fetch details of all channels
	for(var i=0; i<maxChannels; i++){
		channels.push(CommonF.getChannelId(i+1));
	}
	async.each(channels,function(channel,doneCallback){
		readChannelInfo(channel, function(err,channelInfo){
			if(err){
				urls[channel] = '';
			}
			else{
				urls[channel] = channelInfo.cameraUrl;
			}
			return doneCallback(null);
		});
	},
	function(err){
		response.message.urls = urls;
		response.response = true;
		return callback(response);
	});
}

module.exports.sendAllChannels = sendAllChannels;
module.exports.sendChannelInfo = sendChannelInfo;
module.exports.saveChannelInfo = saveChannelInfo;
module.exports.createChannelInfoFiles = createChannelInfoFiles;
module.exports.getInfoEditRule = getInfoEditRule;
module.exports.sendCameraUrls = sendCameraUrls;
module.exports.getImageNamesOfChannel = getImageNamesOfChannel;
