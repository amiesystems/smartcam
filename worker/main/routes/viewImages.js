var express = require('express');
var router = express.Router();
var amqp = require('amqplib/callback_api');
var CommonF = require('../CommonFunctions.js');

var RpcQuery = require('../../resource.json').RpcQuery;

var DEBUG = true;

var replyToQue = 'amq.rabbitmq.reply-to';
var mainQue = 'rpc_main';

var channelId = '';
var fileNames = '';
var errorMsg= '';
var successMsg = '';

function clearContents(){
    fileNames = '';
}

function refreshPage(res){
	res.render('viewImages', { 
		title: 'Event Images',
        channelId: channelId,
        fileNames: fileNames,
		errorMsg: errorMsg,
		successMsg: successMsg
	});
	errorMsg = '';
	successMsg = '';
}


/* GET home page. */
router.get('/', function(req, res, next) {
	channelId = req.query.channelId;
	if(channelId != '' && channelId != undefined){
		var command = CommonF.clone(RpcQuery.getChannelImages);
		command.message.id = channelId;
		//Set error message at the begining
		//Clear if connection successfull
		errorMsg = "Server busy, please try later";
		clearContents();
		//Connect to rabbit mq broker
		amqp.connect('amqp://localhost', function(err, connection){
			connection.createChannel(function(err,channel){
				//Set listener for reply message in noAck mode
				channel.consume(replyToQue, function(msg){
					CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
                    var ipcMessage = JSON.parse(msg.content);
                    console.log("ipcMessage response object in page viewImages");
                    console.dir(ipcMessage);                    
					if(ipcMessage.response == true){
						channelId = ipcMessage.message.id;
                        //channelName = ipcMessage.message.name;
                        fileNames = ipcMessage.message.fileNames;
                        //Clear error message
						errorMsg = '';
					}
					else{
						errorMsg = "Server busy, please try later";
					}
					//close connection
					connection.close();
					//send response to browser
					refreshPage(res);
				},{noAck: true});
				
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	}
	else{
		errorMsg = "Invalid request";
		clearContents();
		refreshPage(res);
	}
});

router.post('/refresh',function(req, res, next){
	channelId = req.body.channelId;
	res.redirect(303,'/?channelId=' + channelId);
});

module.exports = router;
