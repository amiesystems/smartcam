var express = require('express');
var router = express.Router();


var fileName = '';
var channelId = '';
var errorMsg= '';
var successMsg = '';

function refreshPage(res){
	res.render('displayImage', { 
		title: 'Event Image',
        fileName: fileName,
        channelId: channelId,
		errorMsg: errorMsg,
		successMsg: successMsg
	});
	errorMsg = '';
	successMsg = '';
}


/* GET page. */
router.get('/', function(req, res, next) {
    fileName = req.query.fileName;
    channelId = req.query.channelId;
    errorMsg='';
    successMsg = '';
    refreshPage(res);
});


module.exports = router;
