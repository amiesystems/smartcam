var express = require('express');
var router = express.Router();
var amqp = require('amqplib/callback_api');
var CommonF = require('../CommonFunctions.js');

var RpcQuery = require('../../resource.json').RpcQuery;

var DEBUG = true;

var replyToQue = 'amq.rabbitmq.reply-to';
var mainQue = 'rpc_main';

var channelId = '';
var channelName = '';
var selectedCamera = '';
var rules = [];
var cameras = [];
var errorMsg= '';
var successMsg = '';

function clearContents(){
	channelName = '';
	selectedCamera = '';
	rules = {};
	cameras = [];
}

function refreshPage(res){
	res.render('index', { 
		title: 'Smart Camera',
		channelId: channelId,
		channelName: channelName,
		selectedCamera: selectedCamera,
		rules: rules,
		cameras: cameras,
		errorMsg: errorMsg,
		successMsg: successMsg
	});
	errorMsg = '';
	successMsg = '';
}


/* GET home page. */
router.get('/', function(req, res, next) {
	channelId = req.query.channelId;
	if(channelId != '' && channelId != undefined){
		var command = CommonF.clone(RpcQuery.getChannelInfo);
		command.message.id = channelId;
		//Set error message at the begining
		//Clear if connection successfull
		errorMsg = "Server busy, please try later";
		clearContents();
		//Connect to rabbit mq broker
		amqp.connect('amqp://localhost', function(err, connection){
			connection.createChannel(function(err,channel){
				//Set listener for reply message in noAck mode
				channel.consume(replyToQue, function(msg){
					CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
					var ipcMessage = JSON.parse(msg.content);
					if(ipcMessage.response == true){
						channelId = ipcMessage.message.id;
						channelName = ipcMessage.message.name;
						selectedCamera = ipcMessage.message.selectedCamera;
						rules = ipcMessage.message.rules;
						cameras = ipcMessage.message.cameras;
						//Clear error message
						errorMsg = '';
					}
					else{
						errorMsg = "Server busy, please try later";
					}
					//close connection
					connection.close();
					//send response to browser
					refreshPage(res);
				},{noAck: true});
				
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	}
	else{
		errorMsg = "Invalid request";
		clearContents();
		refreshPage(res);
	}
});

router.post('/refresh',function(req, res, next){
	channelId = req.body.channelId;
	res.redirect(303,'/?channelId=' + channelId);
});

router.post('/save',function(req, res, next){
	channelId = req.body.channelId;
	channelName = req.body.channelName;
	selectedCamera = req.body.camera;
	//Set error message at the begining
	//Clear if connection successfull
	errorMsg = "Server busy, please try later";
	var command = CommonF.clone(RpcQuery.saveChannelInfo);
	command.message.id = channelId;
	command.message.name = channelName;
	//Fetch details of selected camera from array
	command.message.selectedCamera = '';
	for(var i=0; i<cameras.length; i++){
		if(cameras[i].ID == selectedCamera){
			command.message.selectedCamera = cameras[i];
			break;
		}
	}
	//Connect to rabbit mq broker
	amqp.connect('amqp://localhost', function(err, connection){
		connection.createChannel(function(err,channel){
			//Set listener for reply message in noAck mode
			channel.consume(replyToQue, function(msg){	
				var ipcMessage = JSON.parse(msg.content);
				if(ipcMessage.response == true){
					successMsg = 'Saved configurations'
					//Clear error message
					errorMsg = '';
				}
				else{
					errorMsg = 'Error saving configurations';
				}
				//close connection
				connection.close();
				//send response to browser
				refreshPage(res);
			},
			{noAck: true},
			function(err,options){
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	});
});

router.post('/remove',function(req,res,next){
	var selectedRule = req.body.selectedRule;
	channelId = req.body.channelId;
	console.log("selected rule to be deleted: " + selectedRule, " channel id : " + channelId);
	//Set error message at the begining
	//Clear if connection successfull
	errorMsg = "Server busy, please try later";
	var command = CommonF.clone(RpcQuery.deleteRule);
	command.message.ruleId = selectedRule;
	//Connect to rabbit mq broker
	amqp.connect('amqp://localhost', function(err, connection){
		connection.createChannel(function(err,channel){
			//Send message in notification mode
			//Set listener for reply message in noAck mode
			channel.consume(replyToQue, function(msg){	
				var ipcMessage = JSON.parse(msg.content);
				if(ipcMessage.response == true){
					successMsg = 'Deleted rule'
					//Clear error message
					errorMsg = '';
				}
				else{
					errorMsg = 'Error deleting rule';
				}
				//close connection
				connection.close();
				//send response to browser
				res.redirect(303,'/?channelId=' + channelId);
			},
			{noAck: true},
			function(err,options){
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	});
});

module.exports = router;
