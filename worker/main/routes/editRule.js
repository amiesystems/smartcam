var express = require('express');
var router = express.Router();
var amqp = require('amqplib/callback_api');
var CommonF = require('../CommonFunctions.js');

var RpcQuery = require('../../resource.json').RpcQuery;
var Config = require('../../config.json');
var maxRuleMinutes = Config.MaxRuleMinutes;
var DEBUG = true;

var replyToQue = 'amq.rabbitmq.reply-to';
var mainQue = 'rpc_main';

var channelId = '';
var ruleId = '';
var triggers = [];
var groups = [];
var scenes = [];
var actions = [];
var event = '';
var count = '';
var startTime = '';
var stopTime = '';
var selectedDays = '';
var selectedAction = '';
var actValue = '';
var target = '';
var maxCount = 5;
var days = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
var errorMsg= '';
var successMsg = '';
var selectedNotifOption = '';
var notifOptions = [];
var phoneNumber;

function clearContents(){
	//channelId = '';
	ruleId = '';
	triggers = [];
	groups = [];
	scenes = [];
	actions = [];
	event = '';
	count = '';
	startTime = '';
	stopTime = '';
	selectedDays = '';
	selectedAction = '';
	actValue = '';
	target = '';
	notifOptions=[];
	selectedNotifOption='';
	phoneNumber='';
}

function refreshPage(res){
	res.render('editRule', { 
		title: 'Smart Camera',
		channelId: channelId,
		ruleId: ruleId,
		triggers: triggers,
		groups: groups,
		scenes: scenes,
		actions: actions,
		event: event,
		count: count,
		startTime: startTime,
		stopTime: stopTime,
		selectedDays: selectedDays,
		selectedAction: selectedAction,
		actValue:actValue,
		target: target,
		maxCount: maxCount,
		days: days,
		errorMsg: errorMsg,
		successMsg: successMsg,
		notifOptions: notifOptions,
		selectedNotifOption: selectedNotifOption,
		phoneNumber:phoneNumber
	});
	errorMsg = '';
	successMsg = '';
}


/* GET home page. */
router.get('/', function(req, res, next) {
	//console.log("!!!!!!!!!!!!! page loading with count = " + count);
	var refreshValues = false;
	channelId = req.query.channelId;
	if(ruleId != req.query.ruleId){
		//if ruleId changed, forget all old values in the page..
		refreshValues = true;
	}
	ruleId = req.query.ruleId;
	//console.log("Rule ID" + ruleId);
	if(channelId != '' && channelId != undefined){
		var command = CommonF.clone(RpcQuery.getInfoEditRule);
		command.message.channelId = channelId;
		command.message.ruleId = ruleId;
		//Connect to rabbit mq broker
		amqp.connect('amqp://localhost', function(err, connection){
			connection.createChannel(function(err,channel){
				//Set listener for reply message in noAck mode
				channel.consume(replyToQue, function(msg){
					CommonF.log(DEBUG,'Received response', (msg.content));
					var ipcMessage = JSON.parse(msg.content);
					if(ipcMessage.response == true){
						channelId = ipcMessage.message.channelId;
						ruleId = ipcMessage.message.ruleId;
						triggers = ipcMessage.message.triggers;
						actions = ipcMessage.message.actions;
						notifOptions = ipcMessage.message.notifOptions;
						groups = ipcMessage.message.groups;
						scenes = ipcMessage.message.scenes;
						if(selectedAction&&(refreshValues==false)){
							//if selectedAction is already available in the page, as the last value itself, or a user changed value...
						}else{
							selectedAction = ipcMessage.message.selectedAction;//if first time loading...
						}
						//similarly, load the content from message only if not already present in page for below vars too...
						if(event&&(refreshValues==false)){} else event = ipcMessage.message.event;
						if(count&&(refreshValues==false)){} else  count = ipcMessage.message.count;
						if(startTime&&(refreshValues==false)){} else  startTime = ipcMessage.message.startTime;
						if(stopTime&&(refreshValues==false)){} else  stopTime = ipcMessage.message.stopTime;
						if(selectedDays&&(refreshValues==false)){} else  selectedDays = ipcMessage.message.selectedDays;
						if(actValue&&(refreshValues==false)){} else  actValue = ipcMessage.message.actValue;
						if(target&&(refreshValues==false)){} else  target = ipcMessage.message.target;
						if(selectedNotifOption&&(refreshValues==false)){}else selectedNotifOption = ipcMessage.message.selectedNotifOption;
						if(phoneNumber&&(refreshValues==false)){
							if(isNaN(phoneNumber)){
								phoneNumber = ipcMessage.message.phoneNumber;
							}
						}else{
							phoneNumber = ipcMessage.message.phoneNumber;
						}
					}
					else{
						errorMsg = "Server busy, please try later";
					}
					//close connection
					connection.close();
					//console.log("Event: " + event);
					//send response to browser
					refreshPage(res);
				},{noAck: true});
				
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	}
	else{
		errorMsg = "Invalid request";
		clearContents();
		refreshPage(res);
	}
});

//reload all data in the page from the db..
router.post('/reload',function(req,res,next){
	ruleId = req.body.ruleId;
	channelId = req.body.channelId;
	//console.log("reloading page with add data from the db");
	res.redirect(303,'/editRule?channelId=' + channelId + '&ruleId=' + ruleId);
});

//refresh all data from the page, without forgeting unsaved user edits
router.post('/refresh',function(req, res, next){
	//save all temporary changes in the page to content variables while refreshing.. 
	selectedAction = req.body.actionSelect;
	selectedNotifOption = req.body.notifySelect;
	phoneNumber = req.body.phoneNumberInput;
	channelId = req.body.channelId;
	ruleId = req.body.ruleId;
	event = req.body.event;
	count = req.body.count;
	startTime = req.body.startTime;
	stopTime = req.body.stopTime;
	selectedDays = [];
	if(typeof(req.body.days) == 'string'){
		selectedDays.push(req.body.days);
	}
	else if(req.body.days == undefined){
		selectedDays = [];
	}
	else{
		selectedDays = req.body.days;
	}
	//selectedAction = req.body.action;
	actValue = req.body.actValue;
	if(actValue == undefined){
		actValue = 'OFF';
	}
	if(typeof(actValue) == 'string'){
		actValue = actValue.toUpperCase();
	}
	target = req.body.target;
	try{
		target = JSON.parse(target);
		target = target.value;
	}
	catch(err){
		//Do nothing
	}
	//console.log("refreshing page without forgeting user edits");
	res.redirect(303,'/editRule?channelId=' + channelId + '&ruleId=' + ruleId);
});

router.post ('/save',function(req,res,next){
	var errored=false;
	//Collect required data from request body
	selectedAction = req.body.actionSelect;
	selectedNotifOption = req.body.notifySelect;
	channelId = req.body.channelId;
	ruleId = req.body.ruleId;
	event = req.body.event;
	count = req.body.count;
	startTime = req.body.startTime;
	stopTime = req.body.stopTime;
	var startTimeMins = parseInt(startTime.substring(0,2))*60 + parseInt(startTime.substring(3,5));
	var stopTimeMins = parseInt(stopTime.substring(0,2))*60 + parseInt(stopTime.substring(3,5));
	var diffInMins = 0;
	if(startTimeMins<stopTimeMins){
		diffInMins = stopTimeMins - startTimeMins;
	}else{
		diffInMins = 24*60 - startTimeMins + stopTimeMins;
	}
	// console.log("Start Time = " + startTimeMins + " Stop Time = " + stopTimeMins);
	// console.log("Diff in mins : " + diffInMins + " Max Allowed: " + maxRuleMinutes);
	if(diffInMins > maxRuleMinutes){
		errored = true;
		errorMsg = "Rule duration cannot be more than " + maxRuleMinutes + " minutes for this channel. You selected " + diffInMins + " minutes. Please reduce it."
		refreshPage(res);
	}

	selectedDays = [];
	if(typeof(req.body.days) == 'string'){
		selectedDays.push(req.body.days);
	}
	else if(req.body.days == undefined){
		selectedDays = [];
	}
	else{
		selectedDays = req.body.days;
	}
	//selectedAction = req.body.action;
	actValue = req.body.actValue;
	if(actValue == undefined){
		actValue = 'OFF';
	}
	if(typeof(actValue) == 'string'){
		actValue = actValue.toUpperCase();
	}
	target = req.body.target;
	try{
		target = JSON.parse(target);
		target = target.value;
	}
	catch(err){
		//Do nothing
	}
	phoneNumber = req.body.phoneNumberInput;
	if(selectedAction == "NOTIFY" && selectedNotifOption=="SMS"){
		if(isNaN(phoneNumber)){
			errored = true;
			errorMsg = "Enter a 10 digit Indian mobile number"
			refreshPage(res);
		}
	}
	console.log("Selected action: " + selectedAction);
	console.log("Selected notify option: " + selectedNotifOption);
	console.log("Given phone number: " + phoneNumber);

	if(errored==false){
		//Construct command for sending to main
		var command = CommonF.clone(RpcQuery.saveRule);
		command.message.channelId = channelId;
		command.message.ruleId = ruleId;
		command.message.event = event;
		command.message.count = count;
		command.message.startTime = startTime;
		command.message.stopTime = stopTime;
		command.message.selectedDays = selectedDays;
		command.message.selectedAction = selectedAction;
		command.message.selectedNotifOption = selectedNotifOption;
		command.message.phoneNumber = phoneNumber;
		command.message.actValue = actValue;
		command.message.target = target;
		
		//Connect to rabbit mq broker
		amqp.connect('amqp://localhost', function(err, connection){
			connection.createChannel(function(err,channel){
				//Set listener for reply message in noAck mode
				channel.consume(replyToQue, function(msg){
					CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
					var ipcMessage = JSON.parse(msg.content);
					if(ipcMessage.response == true){
						ruleId = ipcMessage.message.ruleId;
						successMsg = 'Saved rule';
					}
					else{
						errorMsg = "Server busy, please try later";
					}
					//close connection
					connection.close();
					//send response to browser
					refreshPage(res);
				},{noAck: true});
				//Send command
				channel.sendToQueue(mainQue, Buffer.from(JSON.stringify(command)),
					{replyTo: replyToQue});
			});
		});
	}
});

module.exports = router;
