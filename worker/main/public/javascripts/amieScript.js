function selectAllDays(checked){
	document.getElementById('checkboxSUN').checked = checked;
	document.getElementById('checkboxMON').checked = checked;
	document.getElementById('checkboxTUE').checked = checked;
	document.getElementById('checkboxWED').checked = checked;
	document.getElementById('checkboxTHU').checked = checked;
	document.getElementById('checkboxFRI').checked = checked;
	document.getElementById('checkboxSAT').checked = checked;
}

function toggleElement(info,actValue){
	if(info == ''){
		info={value:'',dimmable:0,min:0,max:100};
	}
	else{
		info = JSON.parse(info);
	}
	var content;
	var checked;
	if(info.dimmable == 3){
		content = '<input type="color" name="actValue" required=true value="' + actValue +'"/>';
	}
	else if(info.dimmable){ 	//2 or 1
		content = '<div class="range range-primary">'
					+'<input type="range" name="actValue" min=' + info.min + ' max=' + info.max + ' value="' + actValue + '" required=true onChange="rangePrimary.value=value"/>'
					+'<output id="rangePrimary" ' + actValue + '></output>';
	}
	else{		//0
		checked = (actValue=="ON"?"checked":'');
		content = '<label class="switch">'
					+'<input type="checkbox" name="actValue" ' + checked + '/>'
					+'<span class="slider round"></span>'
				+'</label';
	}
	document.getElementById('valueSelector').innerHTML = content;
}

function deleteRule(id){
	document.getElementById('selectedRule').value = id;
	var result = confirm(`Delete rule ${id}?`);
	return result;
}

$(window).load(function() {
	$('#loading').hide();
 });
