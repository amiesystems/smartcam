

var execSync = require('child_process').execSync;
const config = require('../config.json');
const maxChannels = config.Channels;

var CommonF = require('./CommonFunctions.js');
var Communication = require('./Communication.js');
var Channels = require('./Channels.js');

var DEBUG = true;

//Commands and Responses
var Commands = require('../resource.json').Commands;
var Responses = require('../resource.json').Responses;
var RpcQuery = require('../resource.json').RpcQuery;
var RpcResponse = require('../resource.json').RpcResponse;

//RabbitMQ ques
var mainQue = 'rpc_main';
var rulesQue = 'rpc_rules';

var _mqttClient;

//Find localIp and hubIp
var localIp = '';
var hubIp = '';
try{
	localIp = execSync("ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}'");
	localIp = localIp.toString().trim();
	hubIp = localIp.substring(0,localIp.lastIndexOf('.')) + '.220';
}
catch(err){
	localIp = '';
	hubIp = '';
}


//Create channel info files if they not exist
Channels.createChannelInfoFiles();

//Start mqtt communications
function connectToHubMqtt(hubIp, callback){
	Communication.mqttStart(hubIp,function(err,mqttClient){
		if(err){
			//To Do: Handle error
			return callback(err,null);
		}else{
			//MQTT message listener
			mqttClient.on('message',function(topic,payload){
				var topicArr = topic.split('/');
				var topicPart = '/' + topicArr[2] + '/' + topicArr[3];
				var channelId = topic.split('/')[5];
				var req;
				try{
					req = JSON.parse(payload);
				}catch(e){
					req = payload.toString();
				}
				CommonF.log(DEBUG,'Received message:',topic.toString(),CommonF.strTruncate(payload.toString()));
				// switch(topicPart){
				// 	case Communication.Topics.getChannelListTopic:
				// 		Channels.sendAllChannels(localIp);
				// 		break;
				// 	case Communication.Topics.getCameraListTopic:
				// 		Communication.sendCameraList(req.cameras);
				// 		break;
				// 	case Communication.Topics.getGroupListTopic:
				// 		Communication.sendGroupList(req);
				// 		break;
				// 	case Communication.Topics.controlGroupTopic:
				// 		//Communication.forwardToRules(req);
				// 		Communication.passCurrentStatusToRules(req);
				// 		break;
				// }
				if(topic.indexOf(Communication.Topics.setStatTopic)!==-1){
					//console.log("!!!!!!!!!!! smartcam observed state change for g" + topicArr[2] + " to " + payload);
					Communication.passCurrentStatusToRules('g'+ topicArr[2], req);
				}else if(topic.indexOf(Communication.Topics.setSceneTopic)!==-1){
					//console.log("!!!!!!!!!!! smartcam observed scene change for s" + topicArr[2] + " to " + payload);
					if(req == 'ON') req = 100;
					else if(req == 'OFF') req = 0;
					//console.log("REq is now " + req);
					Communication.passCurrentStatusToRules('s' + topicArr[2], req);
				}else if(topic.indexOf(Communication.Topics.getChannelListTopic)!==-1){
					Channels.sendAllChannels(localIp);
				}else if(topic.indexOf(Communication.Topics.getCameraListTopic)!==-1){
					Communication.sendCameraList(req.cameras);
				}else if(topic.indexOf(Communication.Topics.getGroupListTopic)!==-1){
					Communication.sendGroupList(req);
				}
				// else if(topic.indexOf(Communication.Topics.controlGroupTopic)!==-1){
				// 	console.log(">>>>>>>>>>>control group topic received >> " + topic + " >> " + payload);
				// 	//Communication.passCurrentStatusToRules(req);
				// }
			});
			return callback(null,mqttClient);
		}
	});
}

/*********** RabbitMQ RPC Communication with webpage*************/
//Start rabbitmq server
Communication.rabbitmqConnect(mainQue,function(mainChannel){
	mainChannel.consume(mainQue, function(msg){
		var response;
		CommonF.log(DEBUG,'Received IPC message', CommonF.strTruncate(msg.content));
		var req = JSON.parse(msg.content);
		switch(req.action){
			case RpcQuery.getChannelInfo.action:
				Channels.sendChannelInfo(req.message.id,function(response){	//this response is the details about the channel along with
						//... the list of cameras available at main hub, along with their formatted urls.
					mainChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.saveChannelInfo.action:
				Channels.saveChannelInfo(req.message,function(response){
					mainChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Sent response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.getInfoEditRule.action:
				Channels.getInfoEditRule(req.message,function(response){
					mainChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.saveRule.action:
				Communication.sendRuleForSaving(req,function(response){
					mainChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Sent response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.setStatus.action:
				Communication.sendSetStatusToHub(req.message);
				mainChannel.ack(msg);
				break;
			case RpcQuery.getCameraUrls.action:
				Channels.sendCameraUrls(function(response){
					mainChannel.sendToQueue(msg.properties.replyTo, 
						new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to rulesEngine', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
			case RpcQuery.deleteRule.action:
				Communication.forwardToRules(req);
				//Send response
				response = CommonF.clone(RpcQuery.deleteRule);
				response.response = true;
				mainChannel.sendToQueue(msg.properties.replyTo, 
					new Buffer(JSON.stringify(response)));
				mainChannel.ack(msg);
				CommonF.log(DEBUG, 'Send response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				break;
			case RpcQuery.getChannelImages.action:
				Channels.getImageNamesOfChannel(req.message.id,function(response){	//this response is the details about the channel along with
						//... the list of cameras available at main hub, along with their formatted urls.
					mainChannel.sendToQueue(msg.properties.replyTo, new Buffer(JSON.stringify(response)));
					mainChannel.ack(msg);
					CommonF.log(DEBUG, 'Send response to webserver', CommonF.strTruncate(JSON.stringify(response)));
				});
				break;
		}
	});
	CommonF.log(DEBUG,'Started RabbitMQ RPC server: main que');
	//mqtt connection to be made after rabbitmq is setup...
	connectToHubMqtt(hubIp,function(err,mqttClient){
		if(err){
			console.log("error in setting up mqtt connection with the hub");	//todo handle this 
		}else{
			_mqttClient = mqttClient;
			//console.log("Mqtt setup succesfully!");
		}
	});
});
