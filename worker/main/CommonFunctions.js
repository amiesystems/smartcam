const config = require('../config.json');
const deviceId = config.DeviceId;

/*************Utility functions************/
function log(){
	var message;
	//If First argument is true
	if(arguments[0] == true){
		message = '['+ Date() + ']';
		for(var i=1; i<arguments.length; i++){
			message += ' ' + arguments[i];
		}
		console.log(message);
	}
}

//This function truncates large strings
function strTruncate(str,size){
	str = str.toString();
	if(!size){
		size = 40;
	}
	if(str.length > size){
		return (str.slice(0,size) + '...');
	}
	else{
		return str;
	}
}

//function to create a fresh copy of an object 
//(just assigning with '=' will give a reference only 
//reference: http://stackoverflow.com/questions/728360/how-do-i-correctly-clone-a-javascript-object
function clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
};

function getChannelId(index){
	return deviceId + 'C' + index;
}

//Export functions
module.exports.log = log;
module.exports.strTruncate = strTruncate;
module.exports.clone = clone;
module.exports.getChannelId = getChannelId;
