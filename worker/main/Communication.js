var mqtt = require('mqtt');
var mqttClient;
var http = require('http');
var events = require('events');
var eventEmitter = new events.EventEmitter();
//For Rabbitmq
var amqp = require('amqplib/callback_api');

const config = require('../config.json');
const maxChannels = config.Channels;

var hubSourceId = 'amieHub';
var smartCamSourceId = 'smartCam';
var hubId;

var CommonF = require('./CommonFunctions.js');

//Commands and Responses for smart camera device communication
var Commands = require('../resource.json').Commands;
var Responses = require('../resource.json').Responses;
var RpcQuery = require('../resource.json').RpcQuery;

//Events
var cameraListEvent = 'cameraList';
var groupListEvent = 'groupList';

//RabbitMQ Channel
var mainChannel;

var rulesQue = 'rpc_rules';
var replyToQue = 'amq.rabbitmq.reply-to';

//Timeouts
var mqttTimeout = 1500; //ms

const DEBUG = true;

var Topics = {
	fromSmartCamTopic:"dev/fromSmartCam/", // dev/fromSmartCam/<channelId>
	toSmartCameraTopic: "hub/toSmartCam/",	// hub/toSmartCam/<channelId>
	getGroupListTopic: '/info/getGroups',
	getCameraListTopic: '/info/getCameras',
	getActionListTopic: '/info/getActionList',
	getChannelListTopic: '/info/getChannelList',
	controlGroupTopic: '/control/setGroupStatus',
	setStatTopic: 'hub/setStat',	//hub/setStat/<group ID>
	setSceneTopic: 'hub/sceneStatus'	//hub/sceneStatus/<scene ID>
}

//Call this function to initiate rabbitmq amqp connection
function rabbitmqConnect(que,callback){
	amqp.connect('amqp://localhost', function(err, connection){
		connection.createChannel(function(err, channel){
			channel.assertQueue(que, {durable: false});
			channel.prefetch(1);
			mainChannel = channel;
			return callback(channel);
		});
	});
}

function getHubId(hubIp,callback){
	//get hub id
	var receivedData = '';
	http.get('http://' + hubIp + ':3000/hubData/getHubId',function(res){
		res.on('data',function(data){
			receivedData += data;
		});
		res.on('end',function(){
			if(res.statusCode == 200){
				try{
					hubId = JSON.parse(receivedData.substring(4)).hubid;
					return callback(null,hubId);
				}
				catch(err){
					return callback(err);
				}
			}
			else{
				return callback(new Error('Could not reach hub. Status code: ' + res.StatusCode));
			}
		});
	}).on('error',function(err){
		return callback(err);
	});
}

//Call this function to start mqtt communications
function mqttStart(hubIp, callback){
	getHubId(hubIp, function(err, hubId){
		//console.log(err, hubId);
		if(err){
			//To do: handle error
			return callback(err);
		}
		else{
			//Connect to broker
			mqttClient = mqtt.connect({port:1883, host:hubIp});
			//Subscribe to topics on successfull mqtt connection
			mqttClient.on('connect',function(connack){
				//Subscribe to receive commands from hub
				mqttClient.subscribe(hubId + '/' + hubSourceId + Topics.getChannelListTopic,function(err){
					CommonF.log(DEBUG,'Subscribed to', hubId + '/' + hubSourceId + Topics.getChannelListTopic);
				});
				
				//Subscribe to receive responses for each channel
				for(var i=0; i<maxChannels; i++){
					//Group Scene List Topic
					CommonF.log(DEBUG,'Subscribed to', hubId + '/' + hubSourceId + Topics.getGroupListTopic + '/response/' + CommonF.getChannelId(i+1));
					mqttClient.subscribe(hubId + '/' + hubSourceId + Topics.getGroupListTopic + '/response/' + CommonF.getChannelId(i+1));
					//Camera List Topic
					CommonF.log(DEBUG,'Subscribed to', hubId + '/' + hubSourceId + Topics.getCameraListTopic + '/response/' + CommonF.getChannelId(i+1));
					mqttClient.subscribe(hubId + '/' + hubSourceId + Topics.getCameraListTopic + '/response/' + CommonF.getChannelId(i+1));
					//Action List Topic
					CommonF.log(DEBUG,'Subscribed to', hubId + '/' + hubSourceId + Topics.getActionListTopic + '/response/' + CommonF.getChannelId(i+1));
					mqttClient.subscribe(hubId + '/' + hubSourceId + Topics.getActionListTopic + '/response/' + CommonF.getChannelId(i+1));
					//Control group Topic
					CommonF.log(DEBUG,'Subscribed to', hubId + '/' + hubSourceId + Topics.controlGroupTopic + '/response/' + CommonF.getChannelId(i+1));
					mqttClient.subscribe(hubId + '/' + hubSourceId + Topics.controlGroupTopic + '/response/' + CommonF.getChannelId(i+1));
				}
				//Subscribe to all the target's status messages so that the state change can be tracked..
				// getAllTargets(function(targets){
				// 	if(targets){
				// 		for(var i = 0;i<targets.length;i++){
				// 			if(targets[i]){
				// 				if(targets[i].length > 2){
				// 					subscribeToGroupStatus(targets[i]);
				// 				}
				// 			}
				// 		}
				// 	}
				// });
				mqttClient.subscribe(Topics.setStatTopic + '/#',function(){
					//console.log(">>>>>>>>>>>> Subscribed to " + Topics.setStatTopic);
				});
				mqttClient.subscribe(Topics.setSceneTopic + '/#',function(){
					//console.log(">>>>>>>>>>>> Subscribed to " + Topics.setSceneTopic);
				});
				
			});
			return callback(null,mqttClient);
		}
	});
}
// //subscribe to a group or scene status
// function subscribeToGroupStatus(target){
// 	mqttClient.subscribe(Topics.setStatTopic + target);
// 	console.log("subscribed to "+Topics.setStatTopic + target);	//untested
// }
// //unsubscribe from a group or scene status
// function unsubscribeFromTarget(target){
// 	mqttClient.unsubscribe(Topics.setStatTopic + target);
// 	console.log("unsubscribed from " + Topics.setStatTopic + target);
// }

// // unsubscribe From All Group or scene Status
// function unsubscribeFromAllTargets(callback){//though train...  i can subscribe to all targets when starting up.. but how to handle when a target is changed or removed...
	
// }

/******************** MQTT Communication functions******************/
//Publish command to hub
function publishCommand(topicPart, payload){
	var topic = hubId + '/' + smartCamSourceId + topicPart;
	mqttClient.publish(topic,JSON.stringify(payload));
	CommonF.log(DEBUG,'Published', topic,CommonF.strTruncate(JSON.stringify(payload)));
}

//Publish response to hub
function publishResponse(topicPart, payload){
	var topic = hubId + '/' + smartCamSourceId + topicPart + '/response/' + hubSourceId;
	mqttClient.publish(topic,JSON.stringify(payload));
	CommonF.log(DEBUG,'Published', topic,CommonF.strTruncate(JSON.stringify(payload)));
}

//Get camera list from hub
function getCameraList(channelId,callback){
	var timeoutObj;
	var payload = {};
	payload.id = channelId;
	//Set event listener for cameraList response - t
	eventEmitter.once(cameraListEvent,function(cameras){//his function gets triggered when 'sendCameraList()is called
		if(timeoutObj){
			clearTimeout(timeoutObj);
		}
		return callback(cameras);
	});
	
	//Set timeout to clear event listener
	timeoutObj = setTimeout(function(){
		var cameras = [];
		eventEmitter.removeAllListeners(cameraListEvent);
		return callback(cameras);
	},mqttTimeout);
	//Publish message
	publishCommand(Topics.getCameraListTopic,payload);
}

//Emit event to send camera list
function sendCameraList(cameras){
	eventEmitter.emit(cameraListEvent,cameras);
}

//Get list of groups and scenes from hub
function getGroupList(channelId,callback){
	var timeoutObj;
	var payload = {};
	payload.id = channelId;
	//Set event listener for grouplist response
	eventEmitter.once(groupListEvent,function(groups,scenes){
		if(timeoutObj){
			clearTimeout(timeoutObj);
		}
		return callback(groups,scenes);
	});
	
	//Set timeout to clear event listener
	timeoutObj = setTimeout(function(){
		var groups = [];
		var scenes = [];
		eventEmitter.removeAllListeners(groupListEvent);
		return callback(groups,scenes);
	},mqttTimeout);
	//Publish message
	publishCommand(Topics.getGroupListTopic, payload);
}

//Emit event to send group list
function sendGroupList(response){
	eventEmitter.emit(groupListEvent,response.groups, response.scenes);
}

//Get rule object from ruleEngine
function getRule(ruleId, callback){
	var query = CommonF.clone(RpcQuery.getRule);
	query.message.ruleId = ruleId;
	//Set listener for reply message in noAck mode
	mainChannel.consume(replyToQue, function(msg){
		CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
		//close consumer
		mainChannel.cancel(msg.fields.consumerTag);
		return callback(JSON.parse(msg.content).message);
	},
	{noAck: true},		//options
	function(err,options){		//Callback after creating consumer
		//Send query to rules que
		mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(query)),
			{replyTo: replyToQue});
	});
}

//Get rule object from ruleEngine
function getAllRules(channelId, callback){
	var query = CommonF.clone(RpcQuery.getAllRules);
	query.message.channelId = channelId;
	//Set listener for reply message in noAck mode
	mainChannel.consume(replyToQue, function(msg){
		CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
		//close consumer
		mainChannel.cancel(msg.fields.consumerTag);
		return callback(JSON.parse(msg.content).message.rules);
	},
	{noAck: true},
	function(err,options){
		//Send query to rules que
		mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(query)),
			{replyTo: replyToQue});
	});
}
//get all targets from rule engine
function getAllTargets(callback){
	var query = CommonF.clone(RpcQuery.getAllTargets);
	//Set listener for reply message in noAck mode
	mainChannel.consume(replyToQue, function(msg){
		CommonF.log(DEBUG,'Received response', CommonF.strTruncate(msg.content));
		//close consumer
		mainChannel.cancel(msg.fields.consumerTag);
		return callback(JSON.parse(msg.content).message.targets);
	},
	{noAck: true},
	function(err,options){
		//Send query to rules que
		mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(query)),
			{replyTo: replyToQue});
	});
}


function sendRuleForSaving(request, callback){
	//Set listners for reply to que
	mainChannel.consume(replyToQue, function(msg){
		CommonF.log(DEBUG,'Contents saved, response received', CommonF.strTruncate(msg.content));
		//Close consumer
		mainChannel.cancel(msg.fields.consumerTag);
		//Return to caller with response received from ruleEngine
		return callback(JSON.parse(msg.content));
	},
	{noAck: true},
	function(err){
		if(err) throw err;
		CommonF.log("sending content to be saved to rulesEngine");
		//Send command to rulesEngine
		mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(request)),
			{replyTo: replyToQue});
	});
}

function sendSetStatusToHub(request){
	var payload = {};
	payload.value = request.value;
	payload.target = request.target;
	payload.sourceId = request.channelId;
	publishCommand(Topics.controlGroupTopic,payload);
}

function forwardToRules(request){
	//Send command to rules que
	mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(request)));
}

function passCurrentStatusToRules(target, state){
	var payload = CommonF.clone(RpcQuery.currentStatus);
	payload.message.currentValue = state;
	payload.message.target = target;
	//console.dir(payload);
	//Send command to rules que
	mainChannel.sendToQueue(rulesQue, Buffer.from(JSON.stringify(payload)));
}

module.exports.mqttStart = mqttStart;
module.exports.rabbitmqConnect = rabbitmqConnect;
module.exports.publishCommand = publishCommand;
module.exports.publishResponse = publishResponse;
module.exports.getCameraList = getCameraList;
module.exports.sendCameraList = sendCameraList;
module.exports.getGroupList = getGroupList;
module.exports.sendGroupList = sendGroupList;
module.exports.getRule = getRule;
module.exports.getAllRules = getAllRules;
module.exports.sendRuleForSaving = sendRuleForSaving;
module.exports.sendSetStatusToHub = sendSetStatusToHub;
module.exports.forwardToRules = forwardToRules;
module.exports.Topics = Topics;
module.exports.passCurrentStatusToRules = passCurrentStatusToRules;
// module.exports.subscribeToGroupStatus = subscribeToGroupStatus;
