# USAGE
# python real_time_object_detection.py --prototxt MobileNetSSD_deploy.prototxt.txt --model MobileNetSSD_deploy.caffemodel
# this file compares between frames and gives improved updates like 'person entered scene' 'person left scene' etc.

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import datetime
import cv2
#import paho.mqtt.client as mqttClient
import copy
import json
from objdict import ObjDict
import sys
import base64
import urllib
from ClipRecorder import ClipRecorder

#from matplotlib import pyplot as plt
motionThreshold = 1300
detectionThreshold = 600

def motionStatus(status,objects):
    if motionStatus.prevMotion != status:
        #report if previously in different state.
        print("[Info] {} : {}".format(datetime.datetime.now().strftime("%H:%M:%S"), status))
        for i in objects:
            print("    {} in scene".format(CLASSES[i]))
        motionStatus.prevMotion = status
motionStatus.prevMotion = "" #initializing the static attribute prevMotion of the function motionStatus

def GetFrameFromStream(videoStream):
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    f = videoStream.read()
    #print("Ok")
    if f is not None:
        f = imutils.resize(f, width=400)
    return f

def GetGrayFromFrame(f):
    g = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
    g = cv2.GaussianBlur(g, (21, 21), 0)
    return g

def FrameDetection(f):
    blob = cv2.dnn.blobFromImage(cv2.resize(f,(300,300)), 0.007843, (300,300), 127.5)
    net.setInput(blob)
    return net.forward()
def dump(obj):
  for attr in dir(obj):
    print("obj.%s = %r" % (attr, getattr(obj, attr)))

def ReportObjects(detections,h,w):
    processedImage = None
    detectedObjects = []
    #objectsDetected = []
    #pprint(detections)
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with
        # the prediction
        confidence = detections[0, 0, i, 2]
        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > args["confidence"]:
            # extract the index of the class label from the
            # `detections`, then compute the (x, y)-coordinates of
            # the bounding box for the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
            #filter - Identify only animated objects (leave out chairs, plants etc.) - Refer CLASSES declaration for understanding this step
            if (idx == 2 or idx == 3 or idx == 4 or idx == 6 or idx == 7
                    or idx == 8 or idx == 10 or idx == 12 or idx == 13 or idx == 14 or idx == 15
                    or idx == 17):
                newObject = True
                for eachObject in detectedObjects:
                    if(eachObject.event == CLASSES[idx]):
                        #if detected object already in the detectedObjects array, just increment the count
                        eachObject.count = eachObject.count+1
                        newObject = False
                if newObject:   #if detected object is not in the array, create a new object and add it to the array
                    detectedObject = ObjDict()
                    detectedObject.event = CLASSES[idx]
                    detectedObject.count = 1
                    detectedObjects.append(detectedObject)

                #objectsDetected.append(idx)
                #print("[Detected] {}  Time: ".format(label) + datetime.datetime.now().strftime("%H:%M:%S"))
                if visual:
                    #draw the detection on the frame
                    cv2.rectangle(frame, (startX, startY), (endX, endY), COLORS[idx], 2)
                    y = startY - 15 if startY - 15 > 15 else startY + 15
                    cv2.putText(frame, label, (startX, y), cv2.FONT_HERSHEY_DUPLEX, 0.3, COLORS[idx], 2)
                    #cv2.imwrite('detected.jpg',frame)
                    
                    processedImage = base64.b64encode(cv2.imencode('.jpg', frame)[1])

                    #detectedObjects.append(processedImage)
                    #print(imageString)
                    #cv2.imshow("Frame", frame)
                    #cv2.waitKey(20)
                    # if (idx == 8 or idx == 12):
                    #     #capture cat or dog detection and save in a file.
                    #     ReportObjects.saveCount += 1
                    #     params = list()
                    #     params.append(16)#cv2.CV_IMWRITE_PNG_COMPRESSION)
                    #     params.append(8)
                    #     cv2.imwrite("errors_{}.png".format(ReportObjects.saveCount),frame,params)
    return detectedObjects, processedImage
# ReportObjects.saveCount = 0

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-l", "--link", default= "http://amieCam:cam-qriotek@192.168.1.20:80/videostream.cgi?user=amieCam&password=cam-qriotek&channel=0&.mjpg",
		help = "Link to access camera stream. Example: http://username:pwd@10.0.0.124:80/videostream.cgi?user=username&password=pwd&channel=0&.mjpg")
ap.add_argument("-p", "--prototxt", default="mob.proto.txt",
                help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="mob.caffemodel",
                help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.40, help="minimum probability to filter weak detections")
ap.add_argument("-i", "--index", help = "This thread's index to be reported back to the parent process.")
ap.add_argument("-b", "--buffer_size", help = "buffer size for recording clips.",type=int, default=32)
ap.add_argument("-o", "--output", help="path to output directory", default="~/SmartCam/Clips")
ap.add_argument("-k", "--codec", type=str, default="MJPG", help="codec of output video")
args = vars(ap.parse_args())
h=0
w=0
visual = True
# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
#print("[INFO] loading model...")
sys.stdout.flush()
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("args[link] : ")
print(args['link'])
print("[INFO] starting video stream...")
sys.stdout.flush()
#vs = VideoStream("http://amieCam:cam-qriotek@10.0.0.124:80/videostream.cgi?user=amieCam&password=cam-qriotek&channel=0&.mjpg",False,(640,480)).start()
vs = VideoStream(args["link"],False,(640,480)).start()
#vs = VideoStream("http://admin:admin@192.168.1.15:80/videostream.cgi?user=admin&password=admin&channel=0&.mjpg",False,(320,240)).start()
#vs = VideoStream("http://amieCam:cam-qriotek@192.168.1.20:80/videostream.cgi?user=amieCam&password=cam-qriotek&channel=0&.mjpg",False,(320,240)).start()
#wait for camera warmup
time.sleep(1.0)
prevFrame = None
frame = None
avg=None
grayFrame = None
#rec = ClipRecorder(bufSize=args["buffer_size"])
consecFrames = 0
# loop over the frames from the video stream
while 1:
    #print("Starting loop")
    sys.stdout.flush()
    objectsInFrame = []
    processedImage = None
    updateConsecFrames = True
    #time.sleep(.020) #20 millisec break for the processor. 
    frame = GetFrameFromStream(vs)
    if frame is None:
        #print("[Error] Camera Feed Error")
        report = ObjDict()
        report.index = args["index"]
        report.contents = "Camera Feed Error"
        print(report.dumps())
        sys.stdout.flush();
        time.sleep(10)   #wait before retrying
        continue
    frame = imutils.resize(frame, width=400)
    grayFrame = GetGrayFromFrame(frame)
    if prevFrame is None:
        prevFrame = grayFrame #initializing previous frame
    frameDelta = cv2.absdiff(prevFrame,grayFrame)
    prevFrame = grayFrame # update prevFrame after calculating delta
    thresh = cv2.threshold(frameDelta, 5, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if imutils.is_cv2() else contours[1]
#IF Motion
    motionObject = None
    shouldLookForObjects = False
    for cont in contours:
        if cv2.contourArea(cont) > detectionThreshold:
            shouldLookForObjects = True
        if cv2.contourArea(cont) > motionThreshold: #report motion if there is substantial differences greater than this threshold.
            if motionObject is None:
                motionObject = ObjDict()
    
    if shouldLookForObjects == False and motionObject is None:
        updateConsecFrames = True

    if shouldLookForObjects:
        #print("frame detection start")
        detections = FrameDetection(frame)
        #print("frame detection end")
        (h,w) = frame.shape[:2]
        objectsInFrame,processedImage = ReportObjects(detections,h,w)
        consecFrames = 0
        # if not rec.recording:
        #     timestamp = datetime.datetime.now()
        #     p = "{}/{}.avi".format(args["output"], timestamp.strftime("%Y%m%d-%H%M%S"))
        #     rec.start(p, cv2.VideoWriter_fourcc(*args["codec"]), args["fps"])
        
    if motionObject is not None:
        motionObject.event = "motion"
        motionObject.count = 0  #motion object's expected count is zero at the rules engine
        objectsInFrame.append(motionObject)

    # otherwise, no action has taken place in this frame, so
    # increment the number of consecutive frames that contain
    # no action
    if updateConsecFrames:
        consecFrames += 1
 
    # update the key frame clip buffer
#    rec.update(frame)
    
    report = ObjDict()
    report.index = args["index"]
    report.contents = objectsInFrame

    report.image = processedImage
        #print(report.image[:80])
        # # Convert back to binary
        # jpg_original = base64.b64decode(report.image)
        # # Write to a file to show conversion worked
        # with open('test.jpg', 'wb') as f_output:
        #     f_output.write(jpg_original)
    
    if len(objectsInFrame) > 0:
        print(report.dumps())
        sys.stdout.flush()
        sys.stdout.flush()
        sys.stdout.flush()
        sys.stdout.flush()
    sys.stdout.flush()
    
